import ServiceAbstract from "../abstract/Service.abstract";
import {Database, db} from "../Database/Database";
import LoginDto from "../Database/dto/login/login.dto";
import validate from "../validation/validate";
import User from "../ModuleUser/user.entity";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken"
require('dotenv').config();

export default class LoginService extends ServiceAbstract {
    private db: Database;

    constructor() {
        super();
        this.db = db;
    }

    async login(body: LoginDto): Promise<string> {
        await validate(body, LoginDto);
        const {email, password} = body;
        const user: User = await this.db.collections['User'].findOne({email: email}) as User;
        if (!user) throw new Error(`Couldn't find user ${email}`);
        if (!user.confirmed) throw new Error(`User ${email} is not confirmed`);
        if (!await bcrypt.compare(password, user.password)) throw new Error(`Password incorrect`);
        return jwt.sign({user}, process.env.JWT_SECRET, {expiresIn: "30m"});
    }
}