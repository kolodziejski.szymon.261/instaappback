import LoginService from "./login.service";
import LoginDto from "../Database/dto/login/login.dto";
import ControllerAbstract from "../abstract/Controller.abstract";
import Controller from "../controller/decorators/controller";
import {Post} from "../controller/decorators/methods";

@Controller('/login')
export default class LoginController extends ControllerAbstract {
    private service: LoginService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new LoginService();
    }

    @Post('')
    async login(req, res): Promise<void> {
        const body: LoginDto = await this.parseBody(req, res) as LoginDto;
        let token: string;
        try {
            token = await this.service.login(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, token: token}));
    }
}