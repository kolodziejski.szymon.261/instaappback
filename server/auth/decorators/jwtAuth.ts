import {IncomingMessage, ServerResponse} from "http";
import ControllerAbstract from "../../abstract/Controller.abstract";
import * as jwt from "jsonwebtoken";
import {Payload, PUser} from "../../abstract/types";
import HttpStatus from "../../abstract/CodeEnum";
require('dotenv').config();

export default function JwtAuth() {
    return (
        target: Object,
        key: PropertyKey,
        descriptor: PropertyDescriptor
    ) => {
        let originalMethod = descriptor.value;
        descriptor.value = function (...args: any[]) {
            const req: IncomingMessage = args[0];
            const res: ServerResponse = args[1];
            const token: string = req.headers.authorization?.split(' ')[1];
            if (!token) return ControllerAbstract.badRequest(res, 'Unauthorized', HttpStatus.Unauthorized);
            let data;
            try {
                data = jwt.verify(token, process.env.JWT_SECRET);
            } catch (e) {
                return ControllerAbstract.badRequest(res, 'Unauthorized', HttpStatus.Unauthorized);
            }
            const user: PUser = {
                _id: data.user._id,
                email: data.user.email,
                imie: data.user.imie,
                nazwisko: data.user.nazwisko,
            }
            const payload: Payload = {
                user: user,
                iat: data.iat,
                exp: data.exp
            }
            args[0].payload = payload;
            originalMethod!.apply(this, args);
        };
        return descriptor;
    };
}