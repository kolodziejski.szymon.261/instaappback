import {createServer, IncomingMessage, Server, ServerResponse} from "http";
import UserController from "./ModuleUser/user.controller";
import {Database, db} from "./Database/Database";
import LoginController from "./ModuleLogin/login.controller";
import ControllerAbstract from "./abstract/Controller.abstract";
import ImageController from "./ModuleImage/image.controller";
import TagController from "./ModuleTag/tag.controller";
import FilterController from "./ModuleFilter/filter.controller";
import ImageTagController from "./ModuleImage/ModuleImageTag/imageTag.controller";
import ControllerMap from "./controller/decorators/controllerMap";
import MainDecorator from "./controller/decorators/mainDecorator";
import {PathMap} from "./abstract/types";
import GetFileController from "./ModuleGetFile/getFile.controller";
import ProfileController from "./ModuleProfile/profile.controller";

@MainDecorator(3000)
export default class MainServer {
    private readonly port: number;
    private server: Server;

    @ControllerMap('/login')
    private loginController: LoginController = new LoginController();
    @ControllerMap('/api/users')
    private userController: UserController = new UserController()
    @ControllerMap('/api/tags')
    private tagController: TagController = new TagController();
    @ControllerMap('/api/filters')
    private filterController: FilterController = new FilterController();
    @ControllerMap('/api/images/tags')
    private imageTagController: ImageTagController = new ImageTagController();
    @ControllerMap('/api/images')
    private imageController: ImageController = new ImageController();
    @ControllerMap('/api/getfile')
    private getFileController: GetFileController = new GetFileController();
    @ControllerMap('/api/profile')
    private profileController: ProfileController = new ProfileController();

    private db: Database;
    private readonly map: PathMap[];

    constructor(port: number = 3000, map: [] = []) {
        this.port = port;
        this.map = map;
        this.server = createServer(async (req, res) => {
            await this.handleRequest(req, res);
        });
        this.db = db;
        this.db.init().then();
    }

    public start(): void {
        this.server.listen(this.port, () => {
            console.log(`Server is listening on port ${this.port}`);
        });
    }

    private async handleRequest(req: IncomingMessage, res: ServerResponse): Promise<void> {
        console.log(req.method, req.url);
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.setHeader('Access-Control-Allow-Headers', '*');
        res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, PUT, POST, PATCH, DELETE');
        res.setHeader('Access-Control-Max-Age', '2592000');
        if (req.method === "OPTIONS") {
            res.writeHead(204);
            res.end();
            return;
        }

        for (let map of this.map) {
            if (req.url.includes(map.path)) {
                await this[map.propertyKey].handleRequest(req, res);
                return;
            }
        }
        ControllerAbstract.notFound(res);
    }
}
