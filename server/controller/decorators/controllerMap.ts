function ControllerMap(path: string): PropertyDecorator {
    return function(target: any, propertyKey: PropertyKey) {
        if (!target.maps) target.maps = [];
        target.maps.push({path, propertyKey, part: path.split('/').length - 1});
    }
}
export default ControllerMap;