function Method(method: string, endpoint: string) {
    let param: string = null;
    if (endpoint.slice(0, 1) === ":") param = endpoint.split(':')[1];
    return function (target: any, propertyKey: PropertyKey, descriptor: PropertyDescriptor) {
        if (!target.maps) target.maps = [];
        target.maps.push({endpoint, propertyKey, method: method, param})
    }
}

function Delete(endpoint: string): MethodDecorator {
    return Method('DELETE', endpoint);
}

function Get(endpoint: string): MethodDecorator {
    return Method('GET', endpoint);
}

function Patch(endpoint: string): MethodDecorator {
    return Method('PATCH', endpoint);
}

function Post(endpoint: string): MethodDecorator {
    return Method('POST', endpoint);
}

function Put(endpoint: string): MethodDecorator {
    return Method('PUT', endpoint);
}

export {Delete, Get, Patch, Post, Put}