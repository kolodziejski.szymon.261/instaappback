export default function Controller(path: string): ClassDecorator {
    return function (constructor: Function) {
        return constructor.bind(constructor, constructor.prototype.maps, path.split('/').length - 1);
    }
}