function MainDecorator(port: number): ClassDecorator {
    return function (constructor: Function) {
        return constructor.bind(constructor, port, constructor.prototype.maps);
    }
}

export default MainDecorator;