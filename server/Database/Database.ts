import {Collection, Db, MongoClient} from "mongodb";
import User from "../ModuleUser/user.entity";
import Image from "../ModuleImage/image.entity"
import Tag from "../ModuleTag/tag.entity";

class Database {
    public collections: Collection[] = [];
    private readonly client: MongoClient;
    private readonly entities;

    constructor(entities: any[]) {
        const url: string = 'mongodb://127.0.0.1:27017';
        this.client = new MongoClient(url);
        this.entities = entities;
    }

    public async init(): Promise<void> {
        this.connect()
            .then(console.log)
            .catch(console.error);
    }

    public async close(): Promise<void> {
        await this.client.close();
    }

    private async connect(): Promise<string> {
        await this.client.connect();
        const db: Db = this.client.db('instaApp');
        for (let entity of this.entities) {
            this.collections[entity.name] = db.collection(entity.name)
        }
        return "Connected to database";
    }
}

const db: Database = new Database([User, Image, Tag]);
export {db, Database};
