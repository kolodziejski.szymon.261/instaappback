import Required from "../../../validation/decorators/required";

export default class LoginDto {
    @Required()
    email: string;

    @Required()
    password: string;
}