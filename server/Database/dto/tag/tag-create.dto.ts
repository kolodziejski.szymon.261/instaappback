import Required from "../../../validation/decorators/required";
import MinLength from "../../../validation/decorators/minLength";
import IsTag from "../../../validation/decorators/isTag";


export default class TagCreateDto {
    @Required()
    @MinLength(3)
    @IsTag()
    name: string;
}

