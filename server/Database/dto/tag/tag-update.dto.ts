import Required from "../../../validation/decorators/required";
import MinLength from "../../../validation/decorators/minLength";
import IsTag from "../../../validation/decorators/isTag";


export default class TagUpdateDto {
    @Required()
    _id: string;

    @MinLength(3)
    @IsTag()
    name: string;
}

