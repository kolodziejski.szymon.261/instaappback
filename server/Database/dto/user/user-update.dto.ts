import MinLength from "../../../validation/decorators/minLength";
import Required from "../../../validation/decorators/required";
import IsEmail from "../../../validation/decorators/isEmail";

export default class UserUpdateDto {
    @Required()
    _id: string;

    @IsEmail()
    email: string;

    @MinLength(3)
    imie: string;

    @MinLength(3)
    nazwisko: string;

    @MinLength(3)
    password: string;
}