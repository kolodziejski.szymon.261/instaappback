import MinLength from "../../../validation/decorators/minLength";
import Required from "../../../validation/decorators/required";
import IsEmail from "../../../validation/decorators/isEmail";

export default class UserCreateDto {
    @Required()
    @IsEmail()
    email: string;

    @Required()
    @MinLength(3)
    imie: string;

    @Required()
    @MinLength(3)
    nazwisko: string;

    @Required()
    @MinLength(3)
    password: string;
}