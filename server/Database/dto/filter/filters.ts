enum FiltersEnum {
    rotate = 'rotate',
    blur = 'blur',
    grayscale = 'grayscale',
}


type filterOptions = {
    angle: number;
} | {
    px: number;
} | {}
export {FiltersEnum, filterOptions};
