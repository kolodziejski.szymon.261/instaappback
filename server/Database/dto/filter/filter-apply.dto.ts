import Required from "../../../validation/decorators/required";
import IsEnum from "../../../validation/decorators/isEnum";
import {FiltersEnum, filterOptions} from "./filters";
export default class FilterApplyDto {
    @Required()
    _id: string;

    @Required()
    @IsEnum(FiltersEnum, 'filtersEnum')
    name: FiltersEnum;

    @Required()
    options: filterOptions;
}