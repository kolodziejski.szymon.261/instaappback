import MinLength from "../../../validation/decorators/minLength";
import IsEmail from "../../../validation/decorators/isEmail";

export default class ProfileUpdateDto {
    @IsEmail()
    email: string;

    @MinLength(3)
    imie: string;

    @MinLength(3)
    nazwisko: string;

    @MinLength(3)
    password: string;
}