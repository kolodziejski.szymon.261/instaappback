import MinLength from "../../../validation/decorators/minLength";
import MaxLength from "../../../validation/decorators/maxLength";
import Required from "../../../validation/decorators/required";
import IsEnum from "../../../validation/decorators/isEnum";
import StatusEnum from "../../../ModuleImage/StatusEnum";
export default class ImageCreateDto {
    @Required()
    @MinLength(3)
    @MaxLength(20)
    album: string;

    @Required()
    @IsEnum(StatusEnum, 'StatusEnum')
    status: StatusEnum;
}