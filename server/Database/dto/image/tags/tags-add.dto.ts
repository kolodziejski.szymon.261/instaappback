import Required from "../../../../validation/decorators/required";
import IsTags from "../../../../validation/decorators/isTags";

export default class TagsAddDto {
    @Required()
    _id: string;

    @Required()
    @IsTags()
    tags: string[];
}