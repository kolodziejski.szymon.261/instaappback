import Required from "../../../../validation/decorators/required";
import IsTag from "../../../../validation/decorators/isTag";

export default class TagAddDto {
    @Required()
    _id: string;

    @Required()
    @IsTag()
    tag: string;
}