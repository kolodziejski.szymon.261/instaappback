import MinLength from "../../../validation/decorators/minLength";
import MaxLength from "../../../validation/decorators/maxLength";
import Required from "../../../validation/decorators/required";
import IsFileName from "../../../validation/decorators/isFileName";
import IsEnum from "../../../validation/decorators/isEnum";
import StatusEnum from "../../../ModuleImage/StatusEnum";

export default class ImageUpdateDto {
    @Required()
    _id: string;

    @MinLength(3)
    @MaxLength(20)
    album: string;

    @IsFileName()
    name: string;

    @IsEnum(StatusEnum, 'StatusEnum')
    status: StatusEnum;
}