import ServiceAbstract from "../abstract/Service.abstract";
import {Database, db} from "../Database/Database";
import Image from "./image.entity";
import * as formidable from "formidable";
import * as path from 'path';
import * as fs from "fs";
import ImageCreateDto from "../Database/dto/image/image-create.dto";
import validate from "../validation/validate";
import ImageUpdateDto from "../Database/dto/image/image-update.dto";
import {ObjectId} from "mongodb";

export default class ImageService extends ServiceAbstract {
    private db: Database;
    static path: string = path.join(__dirname, '../../../server/uploads/');

    constructor() {
        super();
        this.db = db;
    }

    async getImages(): Promise<Image[]> {
        const images = await this.db.collections['Image'].find();
        return images.toArray();
    }

    async getPublicImages(userId: string): Promise<Image[]> {
        try {
            new ObjectId(userId);
        } catch (e) {
            throw new Error(`Invalid user id: ${userId}`);
        }
        const images = await this.db.collections['Image'].find({$or: [{status: 'public'}, {userId: userId}]});
        return images.toArray();
    }

    async getImage(id: string): Promise<Image> {
        const image: Image = await this.db.collections['Image'].findOne({_id: new ObjectId(id)});
        if (!image) throw new Error(`Couldn't find image with id: ${id}`);
        return image;
    }

    async createImage(req): Promise<Image> {
        return new Promise((resolve, reject) => {
            const form = formidable({multiples: false});
            form.parse(req, async (err, fields, files) => {
                const formBody = fields as unknown as ImageCreateDto;
                    try {
                        await validate(formBody, ImageCreateDto);
                    } catch (e) {
                        reject(e);
                        return;
                    }

                const {album, status} = formBody;
                let albumUrl: string;
                if (typeof album !== "string") {
                    reject('??')
                    return;
                }
                albumUrl = path.join(ImageService.path, album);
                try {
                    fs.readdirSync(albumUrl);
                } catch (e) {
                    await fs.mkdirSync(albumUrl);
                }

                const {file} = files;
                if (!("name" in file)) {
                    reject('??')
                    return;
                }
                let buffer;
                try {
                    buffer = fs.readFileSync(path.join(albumUrl, file.name));
                } catch (e) {
                }
                if (buffer) {
                    reject(`File with name ${file.name} already exists in album ${album}`);
                    return;
                }
                await fs.copyFileSync(file.path, path.join(albumUrl, file.name));

                const body: object = {
                    album: album,
                    originalName: file.name,
                    url: `/${album}/${file.name}`,
                    lastChange: Date.now(),
                    userId: req.payload.user._id,
                    status: status
                }
                const image: Image = new Image(body);
                await this.db.collections['Image'].insertOne(image);
                resolve(image);
            });
        });
    }

    async updateImage(body: ImageUpdateDto): Promise<Image> {
        await validate(body, ImageUpdateDto);
        const {_id, album, name} = body;
        const oldImage: Image = await this.getImage(_id);
        if (!oldImage) throw new Error(`Couldn't find image with id: ${_id}`);
        const image: Image = {...oldImage};

        const timestamp: number = Date.now();
        image.lastChange = timestamp;
        image.history.unshift({status: 'modified', lastModified: timestamp});
        if (name) {
            if (name.split('.').at(-1) !== image.originalName.split('.').at(-1))
                throw new Error(`Extension should be ${image.originalName.split('.').at(-1)}`);
            const pieces: string[] = image.url.split('/');
            pieces.pop();
            pieces.push(name);
            image.url = path.join(...pieces);
        }
        if (album) {
            const pieces: string[] = image.url.split('/');
            const name: string = pieces.at(-1);
            image.album = album;
            image.url = path.join(album, name);
        }

        if (image.url === oldImage.url) return image;

        const albumUrl: string = path.join(ImageService.path, image.album);
        try {
            fs.readdirSync(albumUrl);
        } catch (e) {
            await fs.mkdirSync(albumUrl);
        }

        let potentialImage;
        try {
            potentialImage = await fs.readFileSync(path.join(ImageService.path, image.url));
        } catch (e) {
        }
        if (potentialImage) throw new Error('File already exists');

        await fs.copyFileSync(path.join(ImageService.path, oldImage.url), path.join(ImageService.path, image.url));
        await fs.rmSync(path.join(ImageService.path, oldImage.url));

        await this.db.collections['Image'].updateOne({_id: new ObjectId(_id)}, {$set: image});
        return image;
    }

    async deleteImage(id: string): Promise<Image> {
        const image: Image = await this.getImage(id);
        try {
            await fs.rmSync(path.join(ImageService.path, image.url));
        } catch (e) {
            throw new Error(`Couldn't delete file ${id}`);
        }
        await this.db.collections['Image'].deleteOne({_id: new ObjectId(id)});
        for (let tag of image.tags) {
            await this.db.collections['Tag'].updateOne({_id: new ObjectId(tag)}, {$inc: {popularity: -1}});
        }
        return image;
    }
}