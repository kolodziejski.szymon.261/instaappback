import ControllerAbstract from "../../abstract/Controller.abstract";
import Controller from "../../controller/decorators/controller";
import ImageTagService from "./imageTag.service";
import {Get, Patch, Put} from "../../controller/decorators/methods";
import Image from "../image.entity";
import JwtAuth from "../../auth/decorators/jwtAuth";
import TagAddDto from "../../Database/dto/image/tags/tag-add.dto";
import TagsAddDto from "../../Database/dto/image/tags/tags-add.dto";

@Controller('/api/images/tags')
export default class ImageTagController extends ControllerAbstract {
    private service: ImageTagService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new ImageTagService();
    }

    @Get(':id')
    @JwtAuth()
    async getImageTags(req, res, id): Promise<void> {
        let tags: string[];
        try {
            tags = await this.service.getImageTags(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, tags: tags}));
    }

    @Patch('')
    @JwtAuth()
    async updateTag(req, res): Promise<void> {
        const body: TagAddDto = await this.parseBody(req, res) as TagAddDto;
        let image: Image;
        try {
            image = await this.service.updateTag(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }

    @Put('')
    @JwtAuth()
    async updateMassTag(req, res): Promise<void> {
        const body: TagsAddDto = await this.parseBody(req, res) as TagsAddDto;
        let image: Image;
        try {
            image = await this.service.updateMassTag(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }
}