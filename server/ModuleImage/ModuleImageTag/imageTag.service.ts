import ServiceAbstract from "../../abstract/Service.abstract";
import {Database, db} from "../../Database/Database";
import Image from "../image.entity";
import validate from "../../validation/validate";
import TagAddDto from "../../Database/dto/image/tags/tag-add.dto";
import {ObjectId} from "mongodb";
import TagsAddDto from "../../Database/dto/image/tags/tags-add.dto";
import Tag from "../../ModuleTag/tag.entity";

export default class ImageTagService extends ServiceAbstract {
    private db: Database;

    constructor() {
        super();
        this.db = db;
    }

    async getImageTags(_id: string): Promise<string[]> {
        const image = await this.db.collections['Image'].findOne({_id: new ObjectId(_id)});
        if (!image) throw new Error(`Couldn't find image with id ${_id}`);
        return image.tags;
    }

    async updateTag(body: TagAddDto): Promise<Image> {
        await validate(body, TagAddDto);
        const {tag, _id} = body;
        const tags = await this.db.collections['Tag'].find();
        const arr: Tag[] = await tags.toArray();
        if (!arr.find(t => t.name === tag)) throw new Error(`Tag ${tag} doesn't exist`);
        const image: Image = await this.db.collections['Image'].findOne({_id: new ObjectId(_id)});
        if (!image) throw new Error(`Couldn't find image with id ${_id}`);
        if (image.tags.find(t => t === tag)) throw new Error(`Image already has this tag`);
        image.lastChange = Date.now();
        image.history.unshift({status: 'tag added', lastModified: Date.now()});
        const tagOb = await this.db.collections['Tag'].findOne({name: tag});
        image.tags.push(tagOb._id.toString());
        await this.db.collections['Tag'].updateOne({name: tag}, {$inc: {popularity: 1}});
        await this.db.collections['Image'].updateOne({_id: new ObjectId(_id)}, {$set: image});
        return image;
    }

    async updateMassTag(body: TagsAddDto): Promise<Image> {
        await validate(body, TagsAddDto);
        const {tags, _id} = body;
        const aTags = await this.db.collections['Tag'].find();
        const allTags = await aTags.toArray();

        for (let tag of tags)
            if (!allTags.find(t => t.name === tag)) throw new Error(`Tag ${tag} doesn't exist`);
        const image: Image = await this.db.collections['Image'].findOne({_id: new ObjectId(_id)});
        if (!image) throw new Error(`Couldn't find image with id ${_id}`);
        image.lastChange = Date.now();
        image.history.unshift({status: 'set tags', lastModified: Date.now()});

        const tArr = await this.db.collections['Tag'].find({name: {$in: tags}});
        const tagsArr = await tArr.toArray();

        const newTags = tagsArr.filter(t => !image.tags.find(t2 => t2 === t._id.toString()));
        const oldTags = image.tags.filter(t => !tagsArr.find(t2 => t2._id.toString() === t));

        for (let tag of newTags) {
            await this.db.collections['Tag'].updateOne({_id: new ObjectId(tag._id)}, {$inc: {popularity: 1}});
        }
        for (let id of oldTags) {
            await this.db.collections['Tag'].updateOne({_id: new ObjectId(id)}, {$inc: {popularity: -1}});
        }

        image.tags = tagsArr.map(t => t._id.toString());
        await this.db.collections['Image'].updateOne({_id: new ObjectId(_id)}, {$set: image});
        return image;
    }

}