import StatusEnum from "./StatusEnum";

export default class Image {
    _id: string;
    album: string;
    originalName: string;
    url: string;
    lastChange: number;
    tags: string[];
    status: StatusEnum;
    userId: string;
    history: {
        status: string,
        lastModified: number
    }[]

    constructor(body: object) {
        this._id = body['_id'];
        this.album = body['album'];
        this.originalName = body['originalName'];
        this.url = body['url'];
        this.lastChange = body['lastChange'];
        this.tags = body['tags'] || [];
        this.status = body['status'] || 'private';
        this.userId = body['userId'];
        this.history = [{
            status: 'original',
            lastModified: body['lastChange']
        }];
    }
}
