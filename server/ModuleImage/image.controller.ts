import ControllerAbstract from "../abstract/Controller.abstract";
import Controller from "../controller/decorators/controller";
import ImageService from "./image.service";
import JwtAuth from "../auth/decorators/jwtAuth";
import {Delete, Get, Patch, Post} from "../controller/decorators/methods";
import Image from "./image.entity";
import ImageUpdateDto from "../Database/dto/image/image-update.dto";

@Controller('/api/images')
export default class ImageController extends ControllerAbstract {
    private service: ImageService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new ImageService();
    }

    @Get('')
    @JwtAuth()
    async getImages(req, res): Promise<void> {
        const images: Image[] = await this.service.getImages();
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, images: images}));
    }

    @Get('public')
    @JwtAuth()
    async getPublicImages(req, res): Promise<void> {
        const images: Image[] = await this.service.getPublicImages(req.payload.user._id);
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, images: images}));
    }

    @Get(':id')
    @JwtAuth()
    async getImage(req, res, id): Promise<void> {
        let image: Image;
        try {
                image = await this.service.getImage(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }

    @Post('')
    @JwtAuth()
    async createImage(req, res): Promise<void> {
        let image: Image;
        try {
            image = await this.service.createImage(req)
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message || e);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }

    @Patch('')
    @JwtAuth()
    async updateImage(req, res): Promise<void> {
        const body: ImageUpdateDto = await this.parseBody(req, res) as ImageUpdateDto;
        let image;
        try {
            image = await this.service.updateImage(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }

    @Delete(':id')
    @JwtAuth()
    async deleteImage(req, res, id): Promise<void> {
        let image: Image;
        try {
            image = await this.service.deleteImage(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }

}