export default class ServiceAbstract {
    protected getPart(url: string, part: number): string {
        const parts: string[] = url.split("/");
        return parts[part];
    }
}