import {IncomingMessage} from "http";
import User from "../ModuleUser/user.entity";

type PUser = {
    _id: string,
    email: string,
    imie: string,
    nazwisko: string,
}
type Payload = {
    user: PUser,
    iat: number,
    exp: number,
}

type PRequest = IncomingMessage & { payload: Payload };

type UserConfirmReturn = {user: User, token: string};

type PathMap = {path: string, propertyKey: string, part: number};

export {PUser, Payload, PRequest, UserConfirmReturn, PathMap};