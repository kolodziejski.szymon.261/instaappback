import {IncomingMessage, ServerResponse} from "http";

export default class ControllerAbstract {
    private readonly map: [{ endpoint: string, propertyKey: PropertyKey, method: string, param: string }];
    private readonly part: number;

    constructor(map, part) {
        this.map = map;
        this.part = part + 1;
    }

    static notFound(res: ServerResponse): void {
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.statusCode = 404;
        res.end(JSON.stringify({status: 404, message: 'not found'}));
    }

    static badRequest(res: ServerResponse, msg: string, code: number = 400): void {
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.statusCode = code;
        res.end(JSON.stringify({status: code, message: msg}));
    }

    static getPart(url: string, part: number): string {
        const parts: string[] = url.split("/");
        return parts[part] || '';
    }

    public async handleRequest(req: IncomingMessage, res: ServerResponse): Promise<void> {
        const url = ControllerAbstract.getPart(req.url, this.part);
        for (let map of this.map) {
            if ((url === map.endpoint || map.param) && req.method === map.method) {
                if (!map.param)
                return this[map.propertyKey](req, res);
                const param = ControllerAbstract.getPart(req.url, this.part);
                return this[map.propertyKey](req, res, param)
            }
        }
        ControllerAbstract.notFound(res);
    }

    protected parseBody(req, res): Promise<object> {
        return new Promise((resolve, reject): void => {
            let body: string = '';
            req.on('data', chunk => {
                body += chunk.toString();
            });
            req.on('end', () => {
                let parsed;
                try {
                    parsed = JSON.parse(body);
                } catch (e) {
                    ControllerAbstract.badRequest(res, e.message);
                    return;
                }
                resolve(parsed);
            });
            req.on('error', err => {
                reject(err);
            });
        });
    }
}