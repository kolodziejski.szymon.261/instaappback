import Controller from "../controller/decorators/controller";
import ControllerAbstract from "../abstract/Controller.abstract";
import FilterService from "./filter.service";
import {Get, Patch} from "../controller/decorators/methods";
import JwtAuth from "../auth/decorators/jwtAuth";
import * as sharp from "sharp";
import FilterApplyDto from "../Database/dto/filter/filter-apply.dto";
import Image from "../ModuleImage/image.entity";

@Controller('/api/filters')
export default class FilterController extends ControllerAbstract {
    private service: FilterService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new FilterService();
    }

    @Get('metadata')
    @JwtAuth()
    async getMetadata(req, res) {
        const id: string = ControllerAbstract.getPart(req.url, 4);
        let metadata: sharp.Metadata;
        try {
            metadata = await this.service.getMetadata(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        delete metadata.icc;
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, metadata: metadata}));
    }

    @Patch('')
    @JwtAuth()
    async applyFilter(req, res): Promise<void> {
        const body: FilterApplyDto = await this.parseBody(req, res) as FilterApplyDto;
        let image: Image;
        try {
            image = await this.service.applyFilter(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, image: image}));
    }
}