import ServiceAbstract from "../abstract/Service.abstract";
import {Database, db} from "../Database/Database";
import {ObjectId} from "mongodb";
import ImageService from "../ModuleImage/image.service";
import * as path from "path";
import * as sharp from "sharp";
import Image from "../ModuleImage/image.entity";
import FilterApplyDto from "../Database/dto/filter/filter-apply.dto";
import validate from "../validation/validate";
import {FiltersEnum} from "../Database/dto/filter/filters";

export default class FilterService extends ServiceAbstract {
    private db: Database;

    constructor() {
        super();
        this.db = db;
    }

    async getMetadata(id: string): Promise<sharp.Metadata> {
        let image: Image;
        try {
            image = await this.db.collections['Image'].findOne({_id: new ObjectId(id)});
        } catch (e) {
            throw new Error(`Couldn't find image with id: ${id}`);
        }
        if (!image) throw new Error(`Couldn't find image with id: ${id}`);
        const url: string = path.join(ImageService.path, image.url);
        const metadata = await sharp(url).metadata();
        return metadata;
    }

    async applyFilter(body): Promise<Image> {
        await validate(body, FilterApplyDto);
        let image: Image;
        try {
            image = await this.db.collections['Image'].findOne({_id: new ObjectId(body._id)});
        } catch (e) {
            throw new Error(`Couldn't find image with id: ${body._id}`);
        }
        if (!image) throw new Error(`Couldn't find image with id: ${body._id}`);
        const url: string = path.join(ImageService.path, image.url);
        switch (body.name) {
            case FiltersEnum.rotate: {
                const {angle} = body.options;
                if (!angle || typeof angle !== "number") throw new Error('Angle needs to be a number');
                await sharp(url).rotate(angle).toFile(
                    path.join(ImageService.path, `${image.url.split('.')[0]}-rotated.${image.url.split('.')[1]}`)
                );
                image.url = `${image.url.split('.')[0]}-rotated.${image.url.split('.')[1]}`;
                image._id = null;
                image.history.unshift({status: 'rotated', lastModified: Date.now()});
                image.tags = [];
                const {insertedId} = await this.db.collections['Image'].insertOne(image);
                const newImage = await this.db.collections['Image'].findOne({_id: insertedId});
                return newImage;
            }
                break;
            case FiltersEnum.blur: {
                const {px} = body.options;
                if (!px || typeof px !== "number") throw new Error('Px needs to be a number');
                await sharp(url).blur(px).toFile(
                    path.join(ImageService.path, `${image.url.split('.')[0]}-blurred.${image.url.split('.')[1]}`)
                );
                image.url = `${image.url.split('.')[0]}-blurred.${image.url.split('.')[1]}`;
                image._id = null;
                image.history.unshift({status: 'blurred', lastModified: Date.now()});
                image.tags = [];
                const {insertedId} = await this.db.collections['Image'].insertOne(image);
                const newImage = await this.db.collections['Image'].findOne({_id: insertedId});
                return newImage;
            }
                break;
            case FiltersEnum.grayscale: {
                await sharp(url).grayscale().toFile(
                    path.join(ImageService.path, `${image.url.split('.')[0]}-grayscale.${image.url.split('.')[1]}`)
                );
                image.url = `${image.url.split('.')[0]}-grayscale.${image.url.split('.')[1]}`;
                image._id = null;
                image.history.unshift({status: 'grayscale', lastModified: Date.now()});
                image.tags = [];
                const {insertedId} = await this.db.collections['Image'].insertOne(image);
                const newImage = await this.db.collections['Image'].findOne({_id: insertedId});
                return newImage;
            }
        }
    }
}