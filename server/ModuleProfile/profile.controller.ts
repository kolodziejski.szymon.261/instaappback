import ControllerAbstract from "../abstract/Controller.abstract";
import Controller from "../controller/decorators/controller";
import ProfileService from "./profile.service";
import {Delete, Get, Patch, Post} from "../controller/decorators/methods";
import JwtAuth from "../auth/decorators/jwtAuth";
import User from "../ModuleUser/user.entity";
import ProfileUpdateDto from "../Database/dto/profile/profile-update.dto";

@Controller('/api/profile')
export default class ProfileController extends ControllerAbstract {
    private service: ProfileService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new ProfileService();
    }

    @Get('')
    @JwtAuth()
    async getProfile(req, res): Promise<void> {
        const {_id} = req.payload.user;
        let profile;
        try {
            profile = await this.service.getProfile(_id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, profile: profile}));
    }


    @Patch('')
    @JwtAuth()
    async updateProfile(req, res): Promise<void> {
        const body: ProfileUpdateDto = await this.parseBody(req, res) as ProfileUpdateDto;
        let profile: User;
        try {
            profile = await this.service.updateProfile(body, req.payload.user._id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        if (profile._id === req.payload.user._id) req.payload.user = profile;
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, profile: profile}));
    }

    @Post('picture')
    @JwtAuth()
    async setProfilePicture(req, res): Promise<void> {
        const message = await this.service.setProfilePicture(req);
        if (message === 'error' || !message) {
            ControllerAbstract.badRequest(res, 'Could not upload file');
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, message: message}));
    }

    @Get('picture')
    @JwtAuth()
    async getProfilePicture(req, res): Promise<void> {
        let image;
        try {
            image = await this.service.getProfilePicture(req.payload.user._id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', `image/${image.ext}`, 'charset=utf-8');
        res.end(image.buffer);
    }

    @Get(':id')
    async getProfileById(req, res, id): Promise<void> {
        let image;
        try {
            image = await this.service.getProfilePicture(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', `image/${image.ext}`, 'charset=utf-8');
        res.end(image.buffer);
    }

    @Delete('picture')
    @JwtAuth()
    async deleteProfilePicture(req, res): Promise<void> {
        try {
            await this.service.deleteProfilePicture(req.payload.user._id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, message: 'Successfully deleted file'}));
    }

}
