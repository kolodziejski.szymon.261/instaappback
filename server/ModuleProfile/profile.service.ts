import ServiceAbstract from "../abstract/Service.abstract";
import {Database, db} from "../Database/Database";
import User from "../ModuleUser/user.entity";
import {ObjectId} from "mongodb";
import ProfileUpdateDto from "../Database/dto/profile/profile-update.dto";
import validate from "../validation/validate";
import * as bcrypt from "bcrypt";
import * as formidable from "formidable";
import * as fs from "fs";
import path = require("path");

export default class ProfileService extends ServiceAbstract {
    private db: Database;
    static path: string = path.join(__dirname, '../../../server/profiles/');

    constructor() {
        super();
        this.db = db;
    }

    async getProfile(id: string): Promise<User> {
        const user: User = await this.db.collections['User'].findOne({_id: new ObjectId(id)});
        if (!user) throw new Error(`Couldn't find user ${id}`);
        return user;
    }

    async updateProfile(body: ProfileUpdateDto, id: string): Promise<User> {
        await validate(body, ProfileUpdateDto);
        const oldUser: User = await this.getProfile(id);
        if (!oldUser) throw new Error(`Couldn't find user with id: ${id}`);
        if (body.password)
            body.password = await bcrypt.hash(body.password, 10);
        const user: User = new User(body);
        for (const key in user) if (user[key] === undefined) user[key] = oldUser[key];
        if (oldUser.confirmed) user.confirmed = true;
        let _id: ObjectId;
        try {
            _id = new ObjectId(id);
        } catch (e) {
            throw new Error('Invalid id');
        }
        await this.db.collections['User'].updateOne({_id: _id}, {$set: user});
        return await this.getProfile(id);
    }

    async setProfilePicture(req): Promise<string> {
        return new Promise(async (resolve, reject) => {
            const {_id} = req.payload.user;
            let id;
            try {
                id = new ObjectId(_id);
            } catch (e) {
                resolve('error');
            }
            const user: User = await this.db.collections['User'].findOne({_id: id});
            if (!user) resolve('error');
            const form = formidable({multiples: false});
            form.parse(req, async (err, fields, files) => {
                const file = files['file'];
                if (!file) resolve('error');
                let buffer;
                try {
                    buffer = await fs.readFileSync(path.join(ProfileService.path, user.picture));
                } catch (e) {
                }
                if (buffer && user.picture !== 'default.png')
                    await fs.unlinkSync(path.join(ProfileService.path, user.picture));
                if ("name" in file) user.picture = `${_id}${path.extname(file.name)}`; else resolve('error');
                if ("path" in file) await fs.copyFileSync(file.path, path.join(ProfileService.path, `${_id}${path.extname(file.name)}`));
                else resolve('error');
                await this.db.collections['User'].updateOne({_id: id}, {$set: user});
                resolve('success');
            });
        });
    }

    async getProfilePicture(_id: string): Promise<{ buffer: Buffer, ext: string }> {
        const user = await this.db.collections['User'].findOne({_id: new ObjectId(_id)});
        const p = path.join(ProfileService.path, user.picture);
        let buffer;
        try {
            buffer = await fs.readFileSync(p);
        } catch (e) {
            throw new Error('No profile picture');
        }
        const ext = user.picture.split('.').at(-1);
        return {buffer, ext: ext === 'jpg' ? 'jpeg' : ext};
    }

    async deleteProfilePicture(_id: string): Promise<void> {
        let id;
        try {
            id = new ObjectId(_id);
        } catch (e) {
            throw new Error('Invalid id');
        }
        const user = await this.db.collections['User'].findOne({_id: id});
        if (!user) throw new Error('User not found');
        if (user.picture === 'default.png') throw new Error('Can\'t delete default profile picture');
        const p = path.join(ProfileService.path, user.picture);
        try {
            await fs.unlinkSync(p);
        } catch (e) {
            throw new Error('Couldn\'t delete profile picture');
        }
        user.picture = 'default.png';
        await this.db.collections['User'].updateOne({_id: id}, {$set: user});
    }
}