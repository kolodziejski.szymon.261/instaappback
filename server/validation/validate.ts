export default function validate(body: object, Dto): void {
    const entity: typeof Dto = new Dto();
    for (const key of Object.getOwnPropertyNames(Dto.prototype)) {
        if (key === 'constructor') continue;
        entity[key] = body[key];
    }
}