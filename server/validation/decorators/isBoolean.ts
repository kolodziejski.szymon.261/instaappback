export default function IsBoolean(): PropertyDecorator {
    return function (target: any, propertyKey: string) {
        let value = target[propertyKey];
        const getter = function () {
            return value;
        };
        const oldSetter = Object.getOwnPropertyDescriptor(target, propertyKey)?.set;
        const setter = function (newVal: any) {
            if (oldSetter) oldSetter(newVal);
            if(newVal !== undefined && typeof newVal !== 'boolean') throw new Error(`Property ${propertyKey} must be a boolean`);
            value = newVal;
        };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}