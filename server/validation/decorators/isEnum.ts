export default function IsEnum(enumName: any, name: string): PropertyDecorator {
    return function (target: any, propertyKey: string) {
        let value = target[propertyKey];
        const getter = function () {
            return value;
        };
        const oldSetter = Object.getOwnPropertyDescriptor(target, propertyKey)?.set;
        const setter = function (newVal: any) {
            if (oldSetter) oldSetter(newVal);
            let valid: boolean = false;
            if (newVal === undefined) valid = true;
            for (let key in enumName) {
                if (key === newVal) valid = true;
            }
            if (!valid) throw new Error(`${newVal} must exist in enum ${name}`);
            value = newVal;
        };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}