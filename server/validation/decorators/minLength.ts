export default function MinLength(number: number): PropertyDecorator {
    return function (target: any, propertyKey: string) {
        let value = target[propertyKey];
        const getter = function () {
            return value;
        };
        const oldSetter = Object.getOwnPropertyDescriptor(target, propertyKey)?.set;
        const setter = function (newVal: any) {
            if (oldSetter) oldSetter(newVal);
            if (newVal && newVal.length < number) {
                throw new Error(`${propertyKey} must be at least ${number} characters long`);
            } else {
                value = newVal;
            }
        };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}