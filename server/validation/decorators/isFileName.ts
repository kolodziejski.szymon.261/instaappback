export default function IsFileName(): PropertyDecorator {
    return function (target: any, propertyKey: string) {
        let value = target[propertyKey];
        const getter = function () {
            return value;
        };
        const oldSetter = Object.getOwnPropertyDescriptor(target, propertyKey)?.set;
        const setter = function (newVal: any) {
            if (oldSetter) oldSetter(newVal);
            const arr = newVal.split('.');
            if (arr.at(-1) === arr[0] || arr.at(-1).length > 4) {
                throw new Error(`${newVal} is not a valid filename`);
            } else {
                value = newVal;
            }
        };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}