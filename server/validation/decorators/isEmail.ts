export default function IsEmail(): PropertyDecorator {
    return function (target: any, propertyKey: string) {
        let value = target[propertyKey];
        const getter = function () {
            return value;
        };
        const oldSetter = Object.getOwnPropertyDescriptor(target, propertyKey)?.set;
        const setter = function (newVal: any) {
            if (oldSetter) oldSetter(newVal);
            let valid: boolean = false;
            if (newVal === undefined) valid = true;
            if (newVal && newVal.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) valid = true;
            if (!valid) throw new Error(`${newVal} must be an email`);
            value = newVal;
        };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    }
}