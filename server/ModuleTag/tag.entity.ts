
export default class Tag{
    _id: string;
    name: string;
    popularity: number;

    constructor(body: object) {
        this._id = body['_id'];
        this.name = body['name'];
        this.popularity = body['popularity'] || 0;
    }
}

