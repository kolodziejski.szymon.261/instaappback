import ServiceAbstract from "../abstract/Service.abstract";
import Tag from "./tag.entity";
import {Database, db} from "../Database/Database";
import {ObjectId} from "mongodb";
import TagCreateDto from "../Database/dto/tag/tag-create.dto";
import validate from "../validation/validate";
import TagUpdateDto from "../Database/dto/tag/tag-update.dto";

export default class TagService extends ServiceAbstract {
    private db: Database;

    constructor() {
        super();
        this.db = db;
    }

    async getTags(): Promise<Tag[]> {
        const tags = await this.db.collections['Tag'].find();
        return tags.toArray();
    }

    async getRawTags(): Promise<string[]> {
        const tags: Tag[] = await this.getTags();
        let rawTags: string[] = [];
        for (let tag of tags) {
            rawTags.push(tag.name);
        }
        return rawTags;
    }

    async getTag(id: string): Promise<Tag> {
        const tag: Tag = await this.db.collections['Tag'].findOne({_id: new ObjectId(id)}) as Tag;
        if (!tag) throw new Error(`Couldn't find tag with id: ${id}`);
        return tag;
    }

    async createTag(body: TagCreateDto): Promise<Tag> {
        await validate(body, TagCreateDto);
        const tags: Tag[] = await this.getTags();
        for (let tag of tags) {
            if (tag.name === body.name) {
                throw new Error(`Tag with name ${body.name} already exists`);
            }
        }
        const tag: Tag = new Tag(body);
        await this.db.collections['Tag'].insertOne(tag);
        return tag;
    }

    async updateTag(body: TagUpdateDto): Promise<Tag> {
        await validate(body, TagUpdateDto);
        const tag: Tag = await this.getTag(body._id);
        if (!tag) throw new Error(`Couldn't find tag with id: ${body._id}`);
        const tags: Tag[] = await this.getTags();
        for (let tag of tags) {
            if (tag.name === body.name) {
                throw new Error(`Tag with name ${body.name} already exists`);
            }
        }
        tag.name = body.name;
        await this.db.collections['Tag'].updateOne({_id: new ObjectId(body._id)}, {$set: tag});
        return await this.getTag(body._id);
    }

    async deleteTag(id: string): Promise<Tag> {
        const tag: Tag = await this.getTag(id);
        if (!tag) throw new Error(`Couldn't find tag with id: ${id}`);
        await this.db.collections['Tag'].deleteOne({_id: new ObjectId(id)});
        return tag;
    }
}