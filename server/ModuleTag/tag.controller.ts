import Controller from "../controller/decorators/controller";
import ControllerAbstract from "../abstract/Controller.abstract";
import TagService from "./tag.service";
import {Delete, Get, Patch, Post} from "../controller/decorators/methods";
import JwtAuth from "../auth/decorators/jwtAuth";
import Tag from "./tag.entity";
import TagCreateDto from "../Database/dto/tag/tag-create.dto";
import TagUpdateDto from "../Database/dto/tag/tag-update.dto";

@Controller('/api/tags')
export default class TagController extends ControllerAbstract {
    private service: TagService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new TagService();
    }

    @Get('')
    @JwtAuth()
    async getTags(req, res): Promise<void> {
        const tags: Tag[] = await this.service.getTags();
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, tags: tags}));
    }

    @Get('raw')
    @JwtAuth()
    async getRawTags(req, res): Promise<void> {
        const rawTags: string[] = await this.service.getRawTags();
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, rawTags: rawTags}));
    }

    @Get(':id')
    @JwtAuth()
    async getTag(req, res, id): Promise<void> {
        let tag: Tag;
        try {
            tag = await this.service.getTag(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, tag: tag}));
    }

    @Post('')
    @JwtAuth()
    async createTag(req, res): Promise<void> {
        const body: TagCreateDto = await this.parseBody(req, res) as TagCreateDto;
        let tag: Tag;
        try {
            tag = await this.service.createTag(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, tag: tag}));
    }

    @Patch('')
    @JwtAuth()
    async updateTag(req, res): Promise<void> {
        const body: TagUpdateDto = await this.parseBody(req, res) as TagUpdateDto;
        let tag: Tag;
        try {
            tag = await this.service.updateTag(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, tag: tag}));
    }

    @Delete(':id')
    @JwtAuth()
    async deleteTag(req, res, id): Promise<void> {
        let tag: Tag;
        try {
            tag = await this.service.deleteTag(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, tag: tag}));
    }

}