export default class User {
    _id: string;
    email: string;
    password: string;
    nazwisko: string;
    imie: string;
    confirmed: boolean;
    picture: string;

    constructor(body: object) {
        this.email = body['email']; //musi być podane (dto)
        this.imie = body['imie']; //musi być podane (dto)
        this.nazwisko = body['nazwisko']; //musi być podane (dto)
        this.password = body['password']; //musi być podane (dto)
        this.picture = body['picture'] || 'default.png';
        this.confirmed = false;
    }
}
