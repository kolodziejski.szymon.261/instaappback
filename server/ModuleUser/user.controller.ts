import ControllerAbstract from "../abstract/Controller.abstract";
import UserService from "./user.service";
import User from "./user.entity";
import UserCreateDto from "../Database/dto/user/user-create.dto";
import UserUpdateDto from "../Database/dto/user/user-update.dto";
import Controller from "../controller/decorators/controller";
import {Delete, Get, Patch, Post} from "../controller/decorators/methods";
import JwtAuth from "../auth/decorators/jwtAuth";
import {PRequest, UserConfirmReturn} from "../abstract/types";
import HttpStatus from "../abstract/CodeEnum";

@Controller('/api/users')
export default class UserController extends ControllerAbstract {
    private service: UserService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new UserService();
    }

    @Get('coffee')
    async lol(req, res): Promise<void> {
        res.statusCode = HttpStatus.ImATeapot;
        res.end(JSON.stringify({status: HttpStatus.ImATeapot, message: "No."}));
    }

    @Get('')
    @JwtAuth()
    async getUsers(req: PRequest, res): Promise<void> {
        const users: User[] = await this.service.getUsers();
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, users: users}));
    }

    @Get('confirm')
    async confirmUser(req, res): Promise<void> {
        const token: string = ControllerAbstract.getPart(req.url, 4);
        let user: User;
        try {
            user = await this.service.confirmUser(token);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, user: user}));
    }

    @Get(':id')
    @JwtAuth()
    async getUser(req, res, id): Promise<void> {
        let user: User;
        try {
            user = await this.service.getUser(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, user: user}));
    }

    @Post('')
    async createUser(req, res): Promise<void> {
        const body: UserCreateDto = await this.parseBody(req, res) as UserCreateDto;
        let user: User, token: string;
        try {
           const data: UserConfirmReturn =  await this.service.createUser(body);
           user = data.user;
           token = data.token;
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: HttpStatus.Created, user: user, message: `Aktywuj konto na localhost:3000/api/users/confirm/${token}`}));
    }


    @Patch('')
    @JwtAuth()
    async updateUser(req, res): Promise<void> {
        const body: UserUpdateDto = await this.parseBody(req, res) as UserUpdateDto;
        let user: User;
        try {
            user = await this.service.updateUser(body);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        if (user._id === req.payload.user._id) req.payload.user = user;
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, user: user}));
    }

    @Delete(':id')
    @JwtAuth()
    async deleteUser(req, res, id): Promise<void> {
        let user: User;
        try {
            user = await this.service.deleteUser(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', 'application/json, charset=utf-8');
        res.end(JSON.stringify({status: 200, user: user}));
    }
}