import ServiceAbstract from "../abstract/Service.abstract";
import validate from "../validation/validate";
import User from "./user.entity";
import UserCreateDto from "../Database/dto/user/user-create.dto";
import {ObjectId} from "mongodb";
import {Database, db} from "../Database/Database";
import UserUpdateDto from "../Database/dto/user/user-update.dto";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import {UserConfirmReturn} from "../abstract/types";

require('dotenv').config();

export default class UserService extends ServiceAbstract {
    private db: Database;

    constructor() {
        super();
        this.db = db;
    }

    async getUsers(): Promise<User[]> {
        const users = await this.db.collections['User'].find();
        return users.toArray();
    }

    async getUser(id: string): Promise<User> {
        const user: User = await this.db.collections['User'].findOne({_id: new ObjectId(id)}) as User;
        if (!user) throw new Error(`Couldn't find user with id: ${id}`);
        return user;
    }

    async createUser(body: UserCreateDto): Promise<UserConfirmReturn> {
        await validate(body, UserCreateDto);
        const users: User[] = await this.getUsers();
        if (users.find(u => u.email === body.email)) throw new Error('User already exists');
        body.password = await bcrypt.hash(body.password, 10);
        const user: User = new User(body);
        await this.db.collections['User'].insertOne(user);
        const token = jwt.sign({user}, process.env.JWT_CONFIRM, {expiresIn: "30m"});
        return {user, token: token};
    }

    async confirmUser(token: string): Promise<User> {
        const data = jwt.verify(token, process.env.JWT_CONFIRM);
        if (typeof data === "string") throw new Error('Cannot verify user');
        const body = data.user;
        const user: User = await this.getUser(body._id);
        if (user.confirmed) throw new Error('User already verified');
        user.confirmed = true;
        let id: ObjectId;
        try {
            id = new ObjectId(body._id);
        } catch (e) {
            throw new Error('Invalid id');
        }
        this.db.collections['User'].updateOne({_id: id}, {$set: user});
        return user;
    }

    async updateUser(body: UserUpdateDto): Promise<User> {
        await validate(body, UserUpdateDto);
        const oldUser: User = await this.getUser(body._id);
        if (!oldUser) throw new Error(`Couldn't find user with id: ${body._id}`);
        if (body.password)
            body.password = await bcrypt.hash(body.password, 10);
        const user: User = new User(body);
        for (const key in user) if (user[key] === undefined) user[key] = oldUser[key];
        if (oldUser.confirmed) user.confirmed = true;
        let id: ObjectId;
        try {
            id = new ObjectId(body._id);
        } catch (e) {
            throw new Error('Invalid id');
        }
        await this.db.collections['User'].updateOne({_id: id}, {$set: user});
        return await this.getUser(body._id);
    }

    async deleteUser(id: string): Promise<User> {
        const user: User = await this.getUser(id);
        await this.db.collections['User'].deleteOne({_id: new ObjectId(id)});
        return user;
    }
}