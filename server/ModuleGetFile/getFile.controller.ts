import ControllerAbstract from "../abstract/Controller.abstract";
import Controller from "../controller/decorators/controller";
import GetFileService from "./getFile.service";
import {Get} from "../controller/decorators/methods";

@Controller('/api/getfile')
export default class GetFileController extends ControllerAbstract {
    private service: GetFileService;

    constructor(map = [], part = 1) {
        super(map, part);
        this.service = new GetFileService();
    }

    @Get(':id')
    async getFile(req, res, id): Promise<void> {
        let image;
        try {
            image = await this.service.getImage(id);
        } catch (e) {
            ControllerAbstract.badRequest(res, e.message);
            return;
        }
        res.setHeader('Content-Type', `image/${image.ext}`, 'charset=utf-8');
        res.end(image.buffer);
    }
}