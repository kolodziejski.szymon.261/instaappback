import ServiceAbstract from "../abstract/Service.abstract";
import {Database, db} from "../Database/Database";
import {ObjectId} from "mongodb";
import ImageService from "../ModuleImage/image.service";
import Image from "../ModuleImage/image.entity";
import * as path from "path";
import * as fs from "fs";

export default class GetFileService extends ServiceAbstract {
    private db: Database;

    constructor() {
        super();
        this.db = db;
    }

    async getImage(id: string): Promise<{ buffer: Buffer, ext: string }> {
        const img: Image = await this.db.collections['Image'].findOne({_id: new ObjectId(id)});
        if (!img) throw new Error(`Couldn't find image ${id}`);
        const p = path.join(ImageService.path, img.url);
        const buffer = await fs.readFileSync(p);
        const ext = img.originalName.split('.').at(-1);
        return {buffer, ext: ext === 'jpg' ? 'jpeg' : ext};
    }
}